import { ref, onMounted } from 'vue'
import { ResponseParser } from './responseParser'
import { ACTIVITIES_URL, DEFAULT_PAGE_LIMIT } from './constants'

export const useApi = (apiCall) => {
    const isLoading = ref(true)
    const data = ref(null)
    const error = ref(null)

    onMounted(async () => {
        try {
            data.value = await apiCall()
        } catch (err) {
            error.value = err
        } finally {
            isLoading.value = false
        }
    })

    return { data, isLoading, error }
}

export const fetchTotalResults = async ({ limit }) => {

    try {
        const response = await fetch(ACTIVITIES_URL)
            .then(res => res.json())

        return {
            totalResults: response.length,
            totalPages: Math.ceil(response.length / limit)
        }
    } catch (error) {
        console.error(error)
        throw new Error('Error fetching data from API')
    }
}

export const fetchActivities = async ({ page = null, limit = DEFAULT_PAGE_LIMIT }) => {

    const pageQuery = page
        ? `?${new URLSearchParams({ '_page': page, '_limit': limit })}`
        : ''

    try {
        const results = await fetch(ACTIVITIES_URL + pageQuery)
            .then(res => res.json())

        const { data } = new ResponseParser(results)
            .parseActivities()
            .setFirstStore()

        const totalResults = await fetch(ACTIVITIES_URL)
            .then(res => res.json())

        return {
            results: data,
            totalResults: totalResults.length,
            totalPages: Math.ceil(totalResults.length / limit)
        }

    } catch (error) {
        console.error(error)
        throw new Error('Error fetching data from API')
    }
}

export const fetchActivity = async ({ id }) => {

    try {
        const response = await fetch(ACTIVITIES_URL + id)
            .then(res => res.json())

        // destructure received 'data' object getting its first item
        // and renaming it to 'data'
        const { data: [data] } = new ResponseParser([response])
            .parseActivities()
            .setFirstStore()

        return data
    } catch (error) {
        console.error(error)
        throw new Error('Error fetching data from API')
    }
}
