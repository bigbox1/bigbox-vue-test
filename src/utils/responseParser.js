export class ResponseParser {
    constructor(data) {
        this.data = data || [{}]
    }

    parseActivities() {
        this.data = this.data.map(item => ({
            ...item,
            activity: JSON.parse(item.activity)
        }))

        return this
    }

    setFirstStore() {
        /**
        * Returns the first item from locations array to emulate closest location
         * */
        const parseLocation = (location) => `${location?.province}, ${location?.address}`
        
        this.data = this.data.map(item => ({
            ...item,
            activity: {
                ...item.activity,
                location: parseLocation(item.activity?.locations[0] || '')
            }
        }))

        return this
    }
}