import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Activities from '../views/Activities.vue'
import Activity from '../views/Activity.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/actividades',
    name: 'Activities',
    component: Activities,
    props: (route) => ({
      pageQuery: route.query._page,
      limitQuery: route.query._limit,
    }),
  },
  {
    path: '/actividades/:id',
    name: 'Activity',
    component: Activity
  }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
})

export default router
